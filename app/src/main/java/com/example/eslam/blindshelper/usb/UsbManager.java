package com.example.eslam.blindshelper.usb;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDeviceConnection;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.util.HashMap;

public class UsbManager implements UsbDevice {


    private android.hardware.usb.UsbManager usbManager;
    private android.hardware.usb.UsbDevice device;
    private UsbSerialDevice serial;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";


    private boolean isDeviceConnected = false;
    private Context context;
    UsbSerialInterface.UsbReadCallback callback;

    public UsbManager(Context context, UsbSerialInterface.UsbReadCallback callback) {
        this.context = context;
        usbManager = (android.hardware.usb.UsbManager) context.getSystemService(Context.USB_SERVICE);
    }


    @Override
    public boolean isDeviceAvailable() {
        HashMap<String, android.hardware.usb.UsbDevice> map = usbManager.getDeviceList();
        for (android.hardware.usb.UsbDevice device : map.values()) {
            this.device = device;
            if (device.getProductName() != null && device.getProductName().contains("Arduino")) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void connect() {
        if (device != null && usbManager.hasPermission(device)) {
            UsbDeviceConnection connection = usbManager.openDevice(device);
            serial = UsbSerialDevice.createUsbSerialDevice(device, connection);
            isDeviceConnected = serial.open();
            serial.setBaudRate(9600);
            serial.setDataBits(UsbSerialInterface.DATA_BITS_8);
            serial.setParity(UsbSerialInterface.PARITY_NONE);
            serial.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
            serial.read(callback);
        } else {
            requestPermission();
        }
    }

    private void requestPermission() {
        if (device != null) {
            PendingIntent mPendingIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
            usbManager.requestPermission(device, mPendingIntent);
        }
    }

    @Override
    public void sendData(String msg) {
        serial.write(msg.getBytes());
    }

    @Override
    public void disconnect() {
        serial.syncClose();
    }

    @Override
    public boolean isDeviceConnected() {
        return isDeviceConnected;
    }
}
