package com.example.eslam.blindshelper.bluetooth;


public interface BluetoothDeviceConnection{
    boolean isConnected();
    boolean sendData(String messageToSend);
    boolean disconnect();
    void addCallback(String key, BluetoothCallback callback);
    void removeCallback(String key);

}
