package com.example.eslam.blindshelper;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.eslam.blindshelper.bluetooth.BluetoothCallback;
import com.example.eslam.blindshelper.bluetooth.BluetoothDeviceConnection;
import com.example.eslam.blindshelper.bluetooth.BlutoothConnection;
import com.example.eslam.blindshelper.usb.UsbDevice;
import com.example.eslam.blindshelper.usb.UsbManager;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Rect;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ch.zhaw.facerecognitionlibrary.Helpers.CustomCameraView;
import ch.zhaw.facerecognitionlibrary.Helpers.FileHelper;
import ch.zhaw.facerecognitionlibrary.Helpers.MatOperation;
import ch.zhaw.facerecognitionlibrary.PreProcessor.PreProcessorFactory;
import ch.zhaw.facerecognitionlibrary.Recognition.Recognition;
import ch.zhaw.facerecognitionlibrary.Recognition.RecognitionFactory;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2, TextToSpeech.OnInitListener {
    String DEVICE_NAME = "HC-05";
    BluetoothAdapter bluetoothAdapter;
    BluetoothDeviceConnection blutoothConnection;

    static {
        Timber.i("start initialize opencv");
        if (!OpenCVLoader.initDebug()) {
            // Handle initialization error
            Timber.i("Can not initialize opencv");
        }
    }

    CustomCameraView mRecognitionView;
    Operations operations;

    private Recognition facialRecogonizer;
    private PreProcessorFactory ppF;
    private boolean isNeedToProcess = true;
    private ExecutorService detectorService = Executors.newSingleThreadExecutor();
    private FileHelper fh;
    private BluetoothDeviceConnection bluetoothConnection;
    private UsbDevice arduino;

    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        tts = new TextToSpeech(this, this);

        // Fire off an intent to check if a TTS engine is installed
        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, 1);


        operations = new Operations(getApplicationContext(),
                this::onSpeechTextDetected,
                this::onOcrTextDetected);

        fh = new FileHelper();
        File folder = new File(fh.getFolderPath());
        if (folder.mkdir() || folder.isDirectory()) {
            Timber.i("New directory for photos created");
        } else {
            Timber.i("Photos directory already existing");
        }
//        bluetoothConnection = ((MyApplication) getApplication()).getBlutoothConnection();
        mRecognitionView = findViewById(R.id.RecognitionView);
        mRecognitionView.setCameraIndex(CameraBridgeViewBase.CAMERA_ID_BACK);
        mRecognitionView.setOnClickListener(this::onTouchRecognitionClicked);


        arduino = new UsbManager(getApplicationContext(), this::onUsbIncomingData);
        arduino.isDeviceAvailable();
        arduino.connect();
        findViewById(R.id.usbConnect).setOnClickListener(v -> {
            operations.speak("Hello");

            if (!arduino.isDeviceConnected())
                arduino.connect();
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                tts.setLanguage(Locale.US);

            } else {
                // missing data, install it
                Intent installIntent = new Intent();
                installIntent.setAction(
                        TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

    private void onBluetoothDataIncoming(String incomingData) {
        if (incomingData != null && operations != null && isTTSRunning) {
            operations.speak(incomingData);
            Toast.makeText(this, "Incoming data from Bluetooth: " + incomingData, Toast.LENGTH_SHORT).show();
        }
    }

    private void onTouchRecognitionClicked(View view) {
        operations.speechRecogonizer.listen();
    }

    private void onUsbIncomingData(byte[] bytes) {
        //TODO When data incoming
        String incommingMessage = new String(bytes);
        operations.speak(incommingMessage);
        Timber.i("Incomming data from usb");
        Toast.makeText(this, "Incoming data from USB: " + incommingMessage, Toast.LENGTH_SHORT).show();

    }

    public void onSpeechTextDetected(String txt) {
        if (txt.contains("name") && isNeedToProcess) {
            isNeedToProcess = false;
            mRecognitionView.disableView();
            String personName = txt.replace("person name", "");
            Intent i = new Intent(this, AddPersonActivity.class);
            i.putExtra("PERSON_NAME", personName);
            startActivity(i);
        } else if (txt.equalsIgnoreCase("open light")) {
            bluetoothConnection.sendData("B");
        } else if (txt.equalsIgnoreCase("close light")) {
            bluetoothConnection.sendData("b");
        } else if (txt.equalsIgnoreCase("open the door")) {
            bluetoothConnection.sendData("H");
        } else if (txt.equalsIgnoreCase("close the door")) {
            bluetoothConnection.sendData("h");
        } else if (txt.equalsIgnoreCase("connect")) {
            if (!arduino.isDeviceConnected())
                arduino.connect();
        }
        Timber.i("Text detected is: %s", txt);
    }

    String lastOcrText = "";

    public void onOcrTextDetected(String txt) {
        if (!lastOcrText.equals(txt))
            operations.speak(txt);
        lastOcrText = txt;
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
    }

    @Override
    public void onCameraViewStopped() {
    }

    String lastPersonName = "";

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        Mat imgRgba = inputFrame.rgba();
        try {
            if (isNeedToProcess) {
                operations.detectTextInImage(imgRgba);
            }
            detectorService.execute(() -> {
                Mat img = new Mat();
                imgRgba.copyTo(img);
                List<Mat> images = ppF.getProcessedImage(img, PreProcessorFactory.PreprocessingMode.RECOGNITION);
                Rect[] faces = ppF.getFacesForRecognition();
                if ((images == null || images.size() == 0 || faces == null || faces.length == 0 || !(images.size() == faces.length))
                        || facialRecogonizer == null) {
                } else {
                    try {
                        faces = MatOperation.rotateFaces(imgRgba, faces, ppF.getAngleForRecognition());
                        if (faces != null) {
                            if (faces.length != 0) {
                                for (int i = 0; i < faces.length; i++) {
                                    String name = facialRecogonizer.recognize(images.get(i), "");
                                    if (!lastPersonName.equals(name))
                                        operations.speak("There is a person in front of you, he may be " + name);
                                    lastPersonName = name;
                                    Timber.i("Face recogonized: %s", name);
                                }
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return imgRgba;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isNeedToProcess = false;
        if (mRecognitionView != null) {
            mRecognitionView.disableView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isNeedToProcess = true;
        operations.init();
        mRecognitionView.setCvCameraViewListener(this);
        mRecognitionView.enableView();

        ppF = new PreProcessorFactory(getApplicationContext());

        final android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
        Thread t = new Thread(() -> {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String algorithm = sharedPref.getString("key_classification_method", getResources().getString(R.string.svm));
            try {
                facialRecogonizer = RecognitionFactory.getRecognitionAlgorithm(getApplicationContext(), Recognition.RECOGNITION, algorithm);
            } catch (Exception ex) {
                Log.e("MainActivity", "There are an error in facialRecogonizer:", ex);
            }
        });

        t.start();

        // Wait until Eigenfaces loading thread has finished
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initBluetooth() {

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }

        //Do you make a pair with your bt device ?
        //yes
        //You not make a pair with bt!
        Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
        if (devices.size() == 0) {
            this.blutoothConnection = new BluetoothDeviceConnection() {
                @Override
                public boolean isConnected() {
                    return false;
                }

                @Override
                public boolean sendData(String messageToSend) {
                    return false;
                }

                @Override
                public boolean disconnect() {
                    return false;
                }

                @Override
                public void addCallback(String key, BluetoothCallback callback) {

                }

                @Override
                public void removeCallback(String key) {
                }
            };
        } else {
            for (BluetoothDevice device : devices) {
                if (device.getName().equals(DEVICE_NAME)) {
                    this.blutoothConnection = new BlutoothConnection(device);
                    this.bluetoothConnection = blutoothConnection;
                    this.bluetoothConnection.addCallback("MainActivity", this::onBluetoothDataIncoming);

                    break;
                }
//            if(device.getAddress().equals(DEVICE_ADDRESS)){
//                blutoothConnection = new BlutoothConnection(device);
//                break;
//            }
            }

        }
    }

    boolean isTTSRunning = false;
    @Override
    public void onInit(int status) {
        tts.setLanguage(Locale.US);
        initBluetooth();
        isTTSRunning = true;
        tts.speak("Hello",TextToSpeech.QUEUE_ADD,null);
    }
}
