package com.example.eslam.blindshelper.bluetooth;

public interface BluetoothCallback {
    void onIncomingData(String message);

}
