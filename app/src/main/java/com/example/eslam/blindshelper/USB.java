package com.example.eslam.blindshelper;

import com.example.eslam.blindshelper.bluetooth.BluetoothCallback;
import com.example.eslam.blindshelper.bluetooth.BluetoothDeviceConnection;

public class USB implements BluetoothDeviceConnection {
    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public boolean sendData(String messageToSend) {
        return false;
    }

    @Override
    public boolean disconnect() {
        return false;
    }

    @Override
    public void addCallback(String key, BluetoothCallback callback) {

    }

    @Override
    public void removeCallback(String key) {

    }
}
