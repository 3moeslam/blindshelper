package com.example.eslam.blindshelper;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.preference.PreferenceManager;

import com.example.eslam.blindshelper.bluetooth.BluetoothCallback;
import com.example.eslam.blindshelper.bluetooth.BluetoothDeviceConnection;
import com.example.eslam.blindshelper.bluetooth.BlutoothConnection;

import java.lang.ref.WeakReference;
import java.util.Set;

import timber.log.Timber;

public class MyApplication extends Application {

  //  String DEVICE_ADDRESS = "00:21:13:00:5F:96";
    String DEVICE_NAME="HC-05";
    BluetoothAdapter bluetoothAdapter;
    BluetoothDeviceConnection blutoothConnection;

    public static WeakReference<Context> appContext;
    public Operations operations;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = new WeakReference<>(this);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

    }

    public BluetoothDeviceConnection getBlutoothConnection(){
        return blutoothConnection;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        if(blutoothConnection != null)
            blutoothConnection.disconnect();
    }
}
