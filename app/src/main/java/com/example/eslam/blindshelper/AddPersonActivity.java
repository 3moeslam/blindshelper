package com.example.eslam.blindshelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.core.Mat;
import org.opencv.core.Rect;

import java.io.File;
import java.util.Date;
import java.util.List;

import ch.zhaw.facerecognitionlibrary.Helpers.CustomCameraView;
import ch.zhaw.facerecognitionlibrary.Helpers.FileHelper;
import ch.zhaw.facerecognitionlibrary.Helpers.MatName;
import ch.zhaw.facerecognitionlibrary.Helpers.MatOperation;
import ch.zhaw.facerecognitionlibrary.PreProcessor.PreProcessorFactory;

public class AddPersonActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2{


    private String personName;
    private long timerDiff = 500;
    private long lastTime;
    private PreProcessorFactory ppF;
    private int total;
    private FileHelper fileHelper;
    int numberOfPictures = 12;
    CustomCameraView mRecognitionView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);

        Intent intent = getIntent();
        if(intent != null)
            personName = intent.getStringExtra("PERSON_NAME");
        mRecognitionView = findViewById(R.id.RecognitionView);
        mRecognitionView.setCameraIndex(CameraBridgeViewBase.CAMERA_ID_BACK);
        mRecognitionView.setCvCameraViewListener(this);

        ppF = new PreProcessorFactory(this);
        total = 0;
        fileHelper = new FileHelper();
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
    }

    @Override
    public void onCameraViewStopped() {
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat imgRgba = inputFrame.rgba();
        Mat imgCopy = new Mat();
        imgRgba.copyTo(imgCopy);

        long time = new Date().getTime();
        if(lastTime + timerDiff < time) {
            lastTime = time;
        }
        List<Mat> images = ppF.getCroppedImage(imgCopy);
        if (images != null && images.size() == 1){
            Mat img = images.get(0);
            if(img != null){
                Rect[] faces = ppF.getFacesForRecognition();
                if((faces != null) && (faces.length == 1)) {
                    faces = MatOperation.rotateFaces(imgRgba, faces, ppF.getAngleForRecognition());
                    MatName m = new MatName(personName + "_" + total, img);
                    String wholeFolderPath = FileHelper.TRAINING_PATH + personName;
                    new File(wholeFolderPath).mkdirs();
                    fileHelper.saveMatToImage(m, wholeFolderPath + "/");
                    for (Rect face : faces) {
                        MatOperation.drawRectangleAndLabelOnPreview(imgRgba, face, String.valueOf(total), false);
                    }
                    total++;
                    if(total >= numberOfPictures){
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }
            }
        }
            return imgRgba;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        ppF = new PreProcessorFactory(this);
        mRecognitionView.enableView();
    }

    @Override
    public void onPause()
    {
        if (mRecognitionView != null)
            mRecognitionView.disableView();
        super.onPause();
    }

    public void onDestroy() {
        if (mRecognitionView != null)
            mRecognitionView.disableView();
        super.onDestroy();
    }
}
