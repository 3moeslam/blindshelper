package com.example.eslam.blindshelper;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ch.zhaw.facerecognitionlibrary.Helpers.FileHelper;
import ch.zhaw.facerecognitionlibrary.Helpers.MatName;
import ch.zhaw.facerecognitionlibrary.Helpers.MatOperation;
import ch.zhaw.facerecognitionlibrary.Helpers.PreferencesHelper;
import ch.zhaw.facerecognitionlibrary.PreProcessor.PreProcessorFactory;
import ch.zhaw.facerecognitionlibrary.Recognition.Recognition;
import ch.zhaw.facerecognitionlibrary.Recognition.RecognitionFactory;
import timber.log.Timber;

public class Operations implements TextToSpeech.OnInitListener {

    private Context context;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private TextRecognizer textRecognizer;

    private TextToSpeech tts;

    private OnOcrTextDetected ocrTextDetected;

    private Recognition rec;
    private PreProcessorFactory ppF;
    final SpeechRecogonizer speechRecogonizer;
    private Handler mainHandler;
    public boolean isTTSInit = false;

    Operations(Context context, SpeechRecogonizer.OnTextDetectedListner listener, OnOcrTextDetected ocrTextDetected) {
        mainHandler = new Handler();
        this.context = context;
        textRecognizer = new TextRecognizer.Builder(context).build();
        initializeTTS(context);
        speechRecogonizer = new SpeechRecogonizer(context, listener);
        this.ocrTextDetected = ocrTextDetected;
        initializeRecognition(context);
        ppF = new PreProcessorFactory(context);
        init();
    }

    private void initializeRecognition(Context context) {
        Thread t = new Thread(() -> rec = RecognitionFactory.getRecognitionAlgorithm(context, Recognition.RECOGNITION,
                context.getResources().getString(R.string.eigenfaces)));
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void initializeTTS(Context context) {

        tts = new TextToSpeech(context, this);
        tts.setLanguage(Locale.US);

    }

    public void detectTextInImage(final Mat imgRgba) {
        if (imgRgba != null && imgRgba.cols() > 0 && imgRgba.rows() > 0
                && imgRgba.width() > 0 && imgRgba.height() > 0) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Bitmap x = Bitmap.createBitmap(imgRgba.cols(), imgRgba.rows(), Bitmap.Config.ARGB_8888);
                        Utils.matToBitmap(imgRgba, x);
                        Frame ww = new Frame.Builder().setBitmap(x).build();
                        SparseArray<TextBlock> array = textRecognizer.detect(ww);
//                        Timber.i("Array size is: %d", array.size());
//                        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(x);
//                        fireabseVisionDetector.detectInImage(image).addOnSuccessListener(firebaseVisionText -> {
//                            Timber.i("Text Detected is "+firebaseVisionText.getText());
//                        });
                        if (!textRecognizer.isOperational()) {
                            // Note: The first time that an app using a Vision API is installed on a
                            // device, GMS will download a native libraries to the device in order to do detection.
                            // Usually this completes before the app is run for the first time.  But if that
                            // download has not yet completed, then the above call will not detect any text,
                            // barcodes, or faces.
                            // isOperational() can be used to check if the required native libraries are currently
                            // available.  The detectors will automatically become operational once the library
                            // downloads complete on device.
                            Log.w("Eslam...", "Detector dependencies are not yet available.");

                            // Check for low storage.  If there is low storage, the native library will not be
                            // downloaded, so detection will not become operational.
                            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
                            boolean hasLowStorage = context.registerReceiver(null, lowstorageFilter) != null;
                            Toast.makeText(context, "Google GSM Not working", Toast.LENGTH_LONG).show();
                            if (hasLowStorage) {
                                Toast.makeText(context, "Low Storage", Toast.LENGTH_LONG).show();
                                Log.w("Eslam...", "Low Storage");
                            }
                        }

                        if (array.size() > 0) {
                            for (int i = 0; i < array.size(); i++) {
                                if (array.get(i) != null) {
                                    Timber.i("There are a text detected: " + array.get(i).getValue());
                                    String recogonizedText = array.get(i).getValue();
                                    if (recogonizedText.contains("POUND"))
                                        speak(recogonizedText);
                                    ocrTextDetected.onOcrTextDetected(recogonizedText);
                                }
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
        }
    }

    public String recognizeFace(final Mat imgRgba) {
        Mat img = new Mat();
        imgRgba.copyTo(img);
        String name = "Person ahead";
        List<Mat> images = ppF.getProcessedImage(img, PreProcessorFactory.PreprocessingMode.RECOGNITION);
        Rect[] faces = ppF.getFacesForRecognition();
        if (images != null && images.size() != 0 && faces != null && faces.length != 0 && images.size() == faces.length) {
            faces = MatOperation.rotateFaces(imgRgba, faces, ppF.getAngleForRecognition());
            for (int i = 0; i < faces.length; i++) {
                name = rec.recognize(images.get(i), "");
                Timber.i("Face recogonized: %s", name);
            }
        }
        return name;
    }

    public void speak(String txt) {
            String utteranceId = txt.hashCode() + "";
            tts.speak(txt, TextToSpeech.QUEUE_FLUSH, null, utteranceId);

    }

    public void start() {
    }


    public void init() {
        final Handler handler = new Handler(Looper.getMainLooper());
        Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    if (!Thread.currentThread().isInterrupted()) {
                        PreProcessorFactory ppF = new PreProcessorFactory(context);
                        PreferencesHelper preferencesHelper = new PreferencesHelper(context);
                        String algorithm = preferencesHelper.getClassificationMethod();

                        FileHelper fileHelper = new FileHelper();
                        fileHelper.createDataFolderIfNotExsiting();
                        final File[] persons = fileHelper.getTrainingList();
                        if (persons.length > 0) {
                            Recognition rec = RecognitionFactory.getRecognitionAlgorithm(context, Recognition.TRAINING, context.getResources().getString(R.string.eigenfaces));
                            for (File person : persons) {
                                if (person.isDirectory()) {
                                    File[] files = person.listFiles();
                                    int counter = 1;
                                    for (File file : files) {
                                        if (FileHelper.isFileAnImage(file)) {
                                            Mat imgRgb = Imgcodecs.imread(file.getAbsolutePath());
                                            Imgproc.cvtColor(imgRgb, imgRgb, Imgproc.COLOR_BGRA2RGBA);
                                            Mat processedImage = new Mat();
                                            imgRgb.copyTo(processedImage);
                                            List<Mat> images = ppF.getProcessedImage(processedImage, PreProcessorFactory.PreprocessingMode.RECOGNITION);
                                            if (images == null || images.size() > 1) {
                                                // More than 1 face detected --> cannot use this file for training
                                                continue;
                                            } else {
                                                processedImage = images.get(0);
                                            }
                                            if (processedImage.empty()) {
                                                continue;
                                            }
                                            // The last token is the name --> Folder name = Person name
                                            String[] tokens = file.getParent().split("/");
                                            final String name = tokens[tokens.length - 1];

                                            MatName m = new MatName("processedImage", processedImage);
                                            fileHelper.saveMatToImage(m, FileHelper.DATA_PATH);

                                            rec.addImage(processedImage, name, false);

//                                      fileHelper.saveCroppedImage(imgRgb, ppF, file, name, counter);

                                            // Update screen to show the progress
                                            final int counterPost = counter;
                                            final int filesLength = files.length;

                                            counter++;
                                        }
                                    }
                                }
                            }

                        } else {
                            Thread.currentThread().interrupt();
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        });
        handler.post(thread);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            Timber.i("Text to speech engine started successfully.");
            isTTSInit = true;
        } else {
            Timber.i("Error starting the text to speech engine.");
        }
    }

    public interface OnOcrTextDetected {
        void onOcrTextDetected(String txt);
    }
}
