package com.example.eslam.blindshelper.usb;

public interface UsbDevice {
    void connect();
    void sendData(String msg);
    void disconnect();
    boolean isDeviceConnected();
    boolean isDeviceAvailable();
}
