package com.example.eslam.blindshelper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;

import java.sql.Time;
import java.util.ArrayList;

import timber.log.Timber;

class SpeechRecogonizer {

    private SpeechRecognizer speechRecognizer;
    private Intent speechRecognizerIntent;
    private OnTextDetectedListner textListner;


    SpeechRecogonizer(Context context, OnTextDetectedListner listener) {
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
        speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                context.getPackageName());

        this.textListner = listener;

        SpeechRecognitionListener speechListener = new SpeechRecognitionListener();

        speechRecognizer.setRecognitionListener(speechListener);
    }
    public void listen(){
        speechRecognizer.startListening(speechRecognizerIntent);
    }

    protected class SpeechRecognitionListener implements RecognitionListener {

        @Override
        public void onBeginningOfSpeech() {

        }

        @Override
        public void onBufferReceived(byte[] buffer) {

        }

        @Override
        public void onEndOfSpeech() {
        }

        @Override
        public void onError(int error) {
            Timber.i("on error %d",error);
        }

        @Override
        public void onEvent(int eventType, Bundle params) {
        }

        @Override
        public void onPartialResults(Bundle partialResults) {
        }

        @Override
        public void onReadyForSpeech(Bundle params) {
        }

        @Override
        public void onResults(Bundle results) {
            ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            if (matches != null) {
                for (String txt : matches) {
                    textListner.onTextDetected(txt);
                }
            }
        }

        @Override
        public void onRmsChanged(float rmsdB) {
        }
    }

    public interface OnTextDetectedListner {
        void onTextDetected(String txt);
    }
}
